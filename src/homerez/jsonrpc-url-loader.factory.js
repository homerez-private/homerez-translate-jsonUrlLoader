(function() {
  'use strict';

  angular.module('pascalprecht.translate')
    .factory('jsonrpcUrlLoader', jsonrpcUrlLoader);

  jsonrpcUrlLoader.$inject = ['jsonrpc', '$q']; 

  function jsonrpcUrlLoader(jsonrpc, $q) {

    return function (options) {
      if (!options || !options.method) {
        throw new Error('Couldn\'t use jsonrpcUrlLoader since no method is given!');
      }

      if (!options.categories) {
        throw new Error('Couldn\'t use jsonrpcUrlLoader since no categories are given!');
      }

      var promises = options.categories.map(function(category) {
        return jsonrpc.request(options.method, { category: category, language: options.key, format: 'object' });
      });

      return $q.all(promises);
    };
  }

  jsonrpcUrlLoader.displayName = 'jsonrpcUrlLoader';
})();